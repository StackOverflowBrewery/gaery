#ifndef FERMENTER_H
#define FERMENTER_H

#include "driver/gpio.h"
#include "AutoPID.h"
#include "owb.h"
#include "owb_rmt.h"
#include "ds18b20.h"
#include "nvs_flash.h"
#include "nvs.h"

#define FERMENTER_NUM_TEMP_SENSORS CONFIG_NUM_TEMP_SENSORS
#define FERMENTER_MAX_SETPOINT CONFIG_MAX_SETPOINT
#define FERMENTER_MIN_SETPOINT CONFIG_MIN_SETPOINT

#define FERMENTER_HEATING_PWM_PIN GPIO_NUM_4
#define OWB_GPIO GPIO_NUM_5

typedef struct {
  double setpoint;
  double temperature;
  float temps[FERMENTER_NUM_TEMP_SENSORS];
  float pressure;
  double heaterOutput;
} fermenter_state_t;

// const fermenter_state_t fermenter_state;

class Fermenter {
  public:
    Fermenter();
    void setSetpoint(float temp);
    float getSetpoint();
    float getHeatingPower();
    float getTemperature();
    void loop();
  protected:
    fermenter_state_t state;
    AutoPID* pid;

    OneWireBus* owb;
    owb_rmt_driver_info rmt_driver_info;
    DS18B20_Info * devices[FERMENTER_NUM_TEMP_SENSORS];
    uint8_t ds18b20SensorCount;
    float temperatures[FERMENTER_NUM_TEMP_SENSORS];

    nvs_handle_t nvs;

    uint64_t lastNVSWrite;

    void setPwm(double val);
    void setupPwm();
    void setupSensors();
    void readSensors();

};

#endif