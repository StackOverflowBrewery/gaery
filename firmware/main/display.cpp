#include "display.h"
#include "u8g2_esp32_hal.h"

Display::Display() {
  u8g2_esp32_hal_t u8g2_esp32_hal = U8G2_ESP32_HAL_DEFAULT;
  u8g2_esp32_hal.sda = PIN_SDA;
  u8g2_esp32_hal.scl = PIN_SCL;
  u8g2_esp32_hal.reset = GPIO_NUM_15;
  u8g2_esp32_hal_init(u8g2_esp32_hal);

  u8g2_Setup_ssd1309_i2c_128x64_noname2_f(&this->u8g2, U8G2_R2, u8g2_esp32_i2c_byte_cb, u8g2_esp32_gpio_and_delay_cb);
  u8x8_SetI2CAddress(&u8g2.u8x8, DISPLAY_I2C_ADDRESS);
  u8g2_InitDisplay(&this->u8g2);
  u8g2_SetPowerSave(&this->u8g2, 0);
  u8g2_ClearBuffer ( &this->u8g2 );
}

void Display::updateSetPointTemp(float temp) {
  this->setPointTemp = temp;
}

void Display::updateMeasuredTemp(float temp) {
  this->measuredTemp = temp;
}

void Display::updatePressure(float pressure) {
  this->pressure = pressure;
}

void Display::updatePercentHeating(float heating) {
  this->percentHeating = heating;
}

void Display::render() {
  u8g2_ClearBuffer(&this->u8g2);
  u8g2_SetFontPosBottom(&this->u8g2);
  char textBuf[128];
  sprintf(textBuf,"%.f C/", this->setPointTemp);
  u8g2_SetFont(&this->u8g2, u8g2_font_7x14_tr);
  u8g2_uint_t soll_width =  u8g2_GetStrWidth(&this->u8g2, textBuf);
  u8g2_DrawStr(&this->u8g2, 5, 13, textBuf);
  u8g2_DrawHLine(&this->u8g2, 0, 21, 128);

  sprintf(textBuf, "%.1f C", this->measuredTemp);
  u8g2_uint_t ist_width = u8g2_GetStrWidth(&this->u8g2, textBuf);
  u8g2_SetFont(&this->u8g2, u8g2_font_t0_11_tf);
  u8g2_DrawStr(&this->u8g2, 5+soll_width, 13, textBuf);
  u8g2_DrawLine(&this->u8g2, soll_width + ist_width + 8, 0, soll_width + ist_width + 8, 21);

  sprintf(textBuf, "%.f %%", this->percentHeating);
  u8g2_DrawStr(&this->u8g2, soll_width + ist_width + 14, 13, textBuf);
  u8g2_SetFont(&this->u8g2, u8g2_font_4x6_tf);
  u8g2_DrawStr(&this->u8g2, 5, 19, "Soll");
  u8g2_DrawStr(&this->u8g2, 5+soll_width, 19, "Ist");
  u8g2_DrawStr(&this->u8g2, soll_width + ist_width + 14, 19, "Heizleistung");

  u8g2_SetFontPosTop(&this->u8g2);
  // TODO render missing temps somehow

  u8g2_DrawHLine(&this->u8g2, 0, 30, 128);
  u8g2_SetFontPosTop(&this->u8g2);
  u8g2_SetFont(&this->u8g2, u8g2_font_t0_11_tf);
  if(this->pressure >= 0.0) {
    sprintf(textBuf, "Druck: %.2f Bar", this->pressure);
  } else {
    sprintf(textBuf, "Druck: --");
  }
  u8g2_DrawStr(&this->u8g2, 3, 32, textBuf);

  u8g2_SendBuffer(&this->u8g2);
}