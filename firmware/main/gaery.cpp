/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_log.h"

#include "display.h"
#include "encoder.h"
#include "fermenter.h"
#include "pressure_sensor.h"

#define MIN_SETPOINT 2.0
#define MAX_SETPOINT 39.0

static const char* TAG = "main";

static double setPoint;
static float heatingPower;
static float temperature;
static float pressure;

static Display* display;
static ESP32Encoder enc;
static Fermenter* fermenter;
static PressureSensor *pressureSensor;

void pressureSensorTask(void* param) {
    const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

    for(;;) {
        pressure = pressureSensor->getPressure();
        vTaskDelay(xDelay);
    }
    vTaskDelete(NULL);
};

void uiTask(void* param) {
    ESP_LOGI(TAG, "Initialising UI");
    const TickType_t xDelay = 50 / portTICK_PERIOD_MS;

    enc.attachSingleEdge(GPIO_NUM_19, GPIO_NUM_18);
    enc.setCount((int)setPoint);
    enc.setFilter(1024);

    display = new Display();
    

    for(;;) {
        double val = (double)enc.getCount();
        if(val < MIN_SETPOINT) {
            val = MIN_SETPOINT;
        }
        if(val > MAX_SETPOINT) {
            val = MAX_SETPOINT;
        }
        setPoint = val;
        enc.setCount(val);
        display->updateSetPointTemp((float)val);
        display->updatePercentHeating(heatingPower);
        display->updateMeasuredTemp(temperature);
        display->updatePressure(pressure);
        display->render();
        vTaskDelay(xDelay);
    }
    vTaskDelete(NULL);
};

void fermenterTask(void* param) {
    ESP_LOGI(TAG, "Starting fermentation control task");
    const TickType_t xDelay = 50 / portTICK_PERIOD_MS;

    for(;;) {
        fermenter->setSetpoint((float)setPoint);
        fermenter->loop();
        heatingPower = fermenter->getHeatingPower();
        temperature = fermenter->getTemperature();
        vTaskDelay(xDelay);
    }
    vTaskDelete(NULL);
};

extern "C" void app_main()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    fermenter = new Fermenter();
    setPoint = fermenter->getSetpoint();

    pressureSensor = new PressureSensor();

    xTaskCreate(
        fermenterTask,
        "Fermentation control task",
        10000,
        NULL,
        1,
        NULL);

    xTaskCreate(
        pressureSensorTask,
        "Pressure Sensor task",
        10000,
        NULL,
        1,
        NULL);

    xTaskCreate(
        uiTask, /* Task function. */
        "UI task", /* name of task. */
        10000, /* Stack size of task */
        NULL, /* parameter of the task */
        1, /* priority of the task */
        NULL);

    for(;;) {
        vTaskDelay(10/ portTICK_PERIOD_MS);
    }
};
