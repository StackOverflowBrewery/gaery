# Gäry firmware

This is the firmware for 'Gäry' an open source fermentation controller targeted
towards homebrewers based around an ESP32.

## Building

This project was developed with ESP-IDF 4.2.1 and therefore is only known to be buildable
with ESP-IDF 4.2.1. All required libraries are in the `components` folder either directly
or as a git submodule
