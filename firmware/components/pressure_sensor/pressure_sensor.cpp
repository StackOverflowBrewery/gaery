#include "pressure_sensor.h"
#include "driver/adc.h"
#include "esp_log.h"

#define ADC_PIN GPIO_NUM_35
#define ADC_CHANNEL ADC1_CHANNEL_7
#define ADC_VREF_MV 1086
#define ADC_MAX_RAW 4095.0
#define ADC_VOLTAGE_DIVIDER_R1 15.0
#define ADC_VOLTAGE_DIVIDER_R2 10.0
#define PRESSURE_VCC 5.0
#define ADC_CALIBRATE false
#define ADC_NO_OF_SAMPLES 64

static const char* TAG = "pressure_sensor";

static void check_efuse()
{
    //Check TP is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
        ESP_LOGI(TAG, "eFuse Two Point: Supported");
    } else {
        ESP_LOGI(TAG, "eFuse Two Point: NOT supported");
    }

    //Check Vref is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
        ESP_LOGI(TAG, "eFuse Vref: Supported");
    } else {
        ESP_LOGI(TAG, "eFuse Vref: NOT supported");
    }
}

static const adc_channel_t channel = ADC_CHANNEL_7;
static const adc_atten_t atten = ADC_ATTEN_DB_2_5;
static const adc_unit_t unit = ADC_UNIT_1;

PressureSensor::PressureSensor() {
  check_efuse();
  esp_err_t err;
  adc1_config_width(ADC_WIDTH_BIT_12);
  err = adc1_config_channel_atten(ADC_CHANNEL, atten);
  if(err != ESP_OK) {
    // TODO throw exception
  }
  adc_chars = (esp_adc_cal_characteristics_t*)calloc(1, sizeof(esp_adc_cal_characteristics_t));
  esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, ADC_VREF_MV, adc_chars);
}

float PressureSensor::getPressure() {
  uint32_t pressure_raw = 0;
  for(int i = 0; i < ADC_NO_OF_SAMPLES; i++) {
    pressure_raw += adc1_get_raw(ADC_CHANNEL);
  }
  pressure_raw /= ADC_NO_OF_SAMPLES;
  uint32_t voltage = esp_adc_cal_raw_to_voltage(pressure_raw, adc_chars);
  float voltage_measured = voltage / 1000.0;
  float real_voltage = voltage_measured * (ADC_VOLTAGE_DIVIDER_R1/ADC_VOLTAGE_DIVIDER_R2);
  if(real_voltage < 0.50) {
    // Check if a sensor is connected at all. It should output 0.5V at 0 bar
    return -1.0;
  }
  float pressure = (((real_voltage / PRESSURE_VCC) - 0.1) / 1.6) * 10;
  return pressure;
}
