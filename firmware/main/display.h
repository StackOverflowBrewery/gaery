#ifndef DISPLAY_H
#define DISPLAY_H

#include "u8g2.h"

#define DISPLAY_I2C_ADDRESS 0x3C

// SDA - GPIO21
#define PIN_SDA GPIO_NUM_21
// SCL - GPIO22
#define PIN_SCL GPIO_NUM_22

class Display {
  protected:
    u8g2_t u8g2;
    float setPointTemp = 0.0;
    float measuredTemp = 0.0;
    float pressure = 0.0;
    float percentHeating = 0.0;

  public:
    Display();
    void updateSetPointTemp(float temp);
    void updateMeasuredTemp(float temp);
    void updatePressure(float pressure);
    void updatePercentHeating(float percentHeating);
    void render();

};
#endif