#include "fermenter.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_timer.h"
#include <algorithm>
#include <cstring>

#define LEDC_BASE_FREQ     5000

#define KP 0.05
#define KI 0.003
#define KD 0.0002
#define OUTPUT_MIN 0.0
#define OUTPUT_MAX 100.0
#define PWM_MAX 8191.0

#define DS18B20_RESOLUTION   (DS18B20_RESOLUTION_12_BIT)
#define SAMPLE_PERIOD        (1000)   // milliseconds

const static char* TAG = "fermenter";
const static char* NVS_NAMESPACE = "ferm";

double nvs_read_double(nvs_handle_t nvs, const char* name);
void nvs_write_double(nvs_handle_t nvs, const char* name, double val);

Fermenter::Fermenter() {
  setupPwm();
  setupSensors();

  double kp = KP;
  double ki = KI;
  double kd = KD;

  pid = new AutoPID(&state.temperature, &state.setpoint, &state.heaterOutput, OUTPUT_MIN, OUTPUT_MAX, kp, ki, kd);

  esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvs);
  if (err != ESP_OK) {
      ESP_LOGE(TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
  } else {
    double integral = nvs_read_double(this->nvs, "pid_integral");
    if(integral != 0.0) {
      pid->setIntegral(integral);
    }
    double setpoint = nvs_read_double(this->nvs, "setpoint");
    if(setpoint != 0.0) {
      state.setpoint = setpoint;
    }
  }

  pid->setBangBang(2.0, 0.1);
  pid->setTimeStep(4000);
  pid->setOutputRange(OUTPUT_MIN, OUTPUT_MAX);
}

void Fermenter::setupPwm() {
  ledc_timer_config_t ledc_timer = {};
  ledc_timer.duty_resolution = LEDC_TIMER_13_BIT; // resolution of PWM duty
  ledc_timer.freq_hz = LEDC_BASE_FREQ;                      // frequency of PWM signal
  ledc_timer.speed_mode = LEDC_HIGH_SPEED_MODE;           // timer mode
  ledc_timer.timer_num = LEDC_TIMER_0;            // timer index
  ledc_timer.clk_cfg = LEDC_AUTO_CLK;              // Auto select the source clock
  
  ledc_timer_config(&ledc_timer);

  ledc_channel_config_t ledc_channel = {};
  ledc_channel.channel    = LEDC_CHANNEL_0;
  ledc_channel.duty       = 0;
  ledc_channel.gpio_num   = FERMENTER_HEATING_PWM_PIN;
  ledc_channel.speed_mode = LEDC_HIGH_SPEED_MODE;
  ledc_channel.hpoint     = 0;
  ledc_channel.timer_sel  = LEDC_TIMER_0;
  ledc_channel_config(&ledc_channel);
}

void Fermenter::setupSensors() {
  ESP_LOGI(TAG, "Setting up OneWire bus");

  owb = owb_rmt_initialize(&rmt_driver_info, OWB_GPIO, RMT_CHANNEL_1, RMT_CHANNEL_0);
  owb_use_crc(owb, true);  // enable CRC check for ROM code
  owb_use_parasitic_power(owb, false);

  ESP_LOGI(TAG, "OneWire bus ready, searching devices");

  OneWireBus_ROMCode device_rom_codes[FERMENTER_NUM_TEMP_SENSORS];
  int num_devices = 0;
  OneWireBus_SearchState search_state;
  bool found = false;
  owb_search_first(owb, &search_state, &found);
  while (found && num_devices < FERMENTER_NUM_TEMP_SENSORS)
  {
      device_rom_codes[num_devices] = search_state.rom_code;
      ++num_devices;
      owb_search_next(owb, &search_state, &found);
  }
  ds18b20SensorCount = num_devices;

  ESP_LOGI(TAG, "Found %d devices", ds18b20SensorCount);

  for (int i = 0; i < num_devices; ++i)
  {
      DS18B20_Info * ds18b20_info = ds18b20_malloc();  // heap allocation
      devices[i] = ds18b20_info;

      if (num_devices == 1)
      {
          ESP_LOGI(TAG, "Single device optimisations enabled\n");
          ds18b20_init_solo(ds18b20_info, owb);          // only one device on bus
      }
      else
      {
          ds18b20_init(ds18b20_info, owb, device_rom_codes[i]); // associate with bus and device
      }
      ds18b20_use_crc(ds18b20_info, true);           // enable CRC check on all reads
      ds18b20_set_resolution(ds18b20_info, DS18B20_RESOLUTION);
  }
  ESP_LOGI(TAG, "Initialised %d OneWire devices", ds18b20SensorCount);
}

void Fermenter::readSensors(){
  ESP_LOGD(TAG, "Reading temperature sensors");
  if(ds18b20SensorCount == 0) {
    ESP_LOGW(TAG, "No temperature sensors found");
    return;
  }
  ESP_LOGD(TAG, "Starting conversions");
  ds18b20_convert_all(owb);
  ESP_LOGD(TAG, "Waiting for conversions to complete");
  ds18b20_wait_for_conversion(devices[0]);

  float temp = 0.0;
  for(int i = 0; i < ds18b20SensorCount; i++) {
    DS18B20_ERROR err = ds18b20_read_temp(devices[i], &temperatures[i]);
    if(err != DS18B20_OK) {
      ESP_LOGE(TAG, "Failed to read ds18b20 sensor");
    }
    temp += temperatures[i];
  }
  state.temperature = temp / (double)ds18b20SensorCount;
}

void Fermenter::setSetpoint(float temp) {
  this->state.setpoint = (double)temp;
  nvs_write_double(this->nvs, "setpoint", this->state.setpoint);
}

float Fermenter::getHeatingPower() {
  return (float)(this->state.heaterOutput);
}

float Fermenter::getTemperature() {
  return (float)(this->state.temperature);
}

float Fermenter::getSetpoint() {
  return (float)(this->state.setpoint);
}

void Fermenter::setPwm(double value) {
  uint32_t duty = (uint32_t)((value / OUTPUT_MAX) * PWM_MAX);
  ledc_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, duty);
  ledc_update_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0);
}

void Fermenter::loop() {
  readSensors();
  pid->run();
  setPwm(state.heaterOutput);

  // Write the current integral every 30 minutes to flash
  if(lastNVSWrite+((1000*1000)*1800) < esp_timer_get_time()) {
    double integral = pid->getIntegral();
    nvs_write_double(nvs, "pid_integral", integral);
    lastNVSWrite = esp_timer_get_time();
  }
}

void double2Bytes(double val, uint8_t* bytes_array){
  // Create union of shared memory space
  union {
    double double_variable;
    uint8_t temp_array[8];
  } u;
  // Overite bytes of union with float variable
  u.double_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 8);
}

double bytes2double(uint8_t* bytes_array) {
  union {
    double double_variable;
    uint8_t temp_array[8];
  } u;
  memcpy(u.temp_array, bytes_array, 8);
  return u.double_variable;
}

double nvs_read_double(nvs_handle_t nvs, const char* name) {
  uint8_t buf[8];
  size_t length = 8;
  esp_err_t err = nvs_get_blob(nvs, name, buf, &length);
  if(err != ESP_OK) {
    return 0.0;
  }
  return bytes2double(buf);
}

void nvs_write_double(nvs_handle_t nvs, const char* name, double val) {
  uint8_t buf[8];
  double2Bytes(val, buf);
  nvs_set_blob(nvs, name, buf, 8);
  nvs_commit(nvs);
}
