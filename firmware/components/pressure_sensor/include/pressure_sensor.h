#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include "esp_adc_cal.h"

class PressureSensor {
  public:
    PressureSensor();
    float getPressure();
  protected:
    esp_adc_cal_characteristics_t* adc_chars;
};

#endif