# Gäry

This is Gäry, a smart fermentation controller geared towards homebrewers-

## Supported sensors

* DS18B20 OneWire Temperature sensors (2 connectors available, but more can be supported)
* 5V analog pressure sensor from AliExpress (TODO insert link)

## Features

* Precise temperature control with smart AutoPID and PWM
* Supports 12-24V input Voltage with a load of up to ~5A (absolutely enough to ferment 40L of wort at kveiv temperatures)
* 2 Mosfet output channels with 12-24V and up to 5 A combined
* Use multiple temperature sensors
* Easy to use controls with rotary encoder and OLED display
* Display current pressure
* GPIO pin header for additional extensions
* Pretty compact

## Future features

* WiFi and BLE support in some form
* Use the second mosfet output to control gas valve for smart spunding
* more to come

